package wedoogift.challenge.level2.solution.service;

import org.springframework.stereotype.Service;
import wedoogift.challenge.level2.solution.model.Company;
import wedoogift.challenge.level2.solution.model.Distribution;
import wedoogift.challenge.level2.solution.model.User;
import wedoogift.challenge.level2.solution.utils.Constant;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Date;
import java.util.GregorianCalendar;

@Service
public class DistributionService {

    /**
     * Simple methode which build a new distribution
     * @param id
     * @param company
     * @param user
     * @param amount
     * @param startDate
     * @param type
     * @return
     */
    public Distribution buildNewDistribution(int id, Company company, User user, double amount, Date startDate, int type) {
        Date endDate = calculateEndDate(startDate, type);
        return new Distribution(id, amount, type, startDate, endDate, company.getId(), user.getId());
    }

    /**
     * A method which calculate the end date of the distribution from the start date depending of the distribution type
     * @param startDate
     * @param type 1 for Gift, 2 for Meal
     * @return the calculated end date
     */
    private Date calculateEndDate(Date startDate, int type) {
        LocalDateTime localDateTime = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        switch (type) {
            case Constant.MEAL_TYPE_DISTRIBUTION_ID:
                localDateTime = localDateTime.plusYears(1);
                int endDateYear = localDateTime.getYear();
                YearMonth yearMonthObject = YearMonth.of(endDateYear, Constant.END_DATE_FOR_MEAL_TYPE_DISTRIBUTION_MONTH);
                int endDateMonthDayNumber = yearMonthObject.lengthOfMonth();
                return new GregorianCalendar(endDateYear, Constant.END_DATE_FOR_MEAL_TYPE_DISTRIBUTION_MONTH - 1, endDateMonthDayNumber).getTime();
            case Constant.GIFT_TYPE_DISTRIBUTION_ID:
            default:
                localDateTime = localDateTime.plusDays(Constant.END_DATE_DAY_NUMBER);
                return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        }
    }

}
