package wedoogift.challenge.level2.solution.service;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;
import wedoogift.challenge.level2.solution.model.Company;
import wedoogift.challenge.level2.solution.model.Distribution;
import wedoogift.challenge.level2.solution.model.User;
import wedoogift.challenge.level2.solution.model.Wallet;
import wedoogift.challenge.level2.solution.utils.Constant;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class JsonFileService {

    /**
     * Read the inputJson
     * @return the extracted Json from the file
     * @throws IOException
     * @throws ParseException
     */
    public JSONObject readInputJsonFile() throws IOException, ParseException {
        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader("input.json");
        Object inputFileJsonList = jsonParser.parse(reader);
        JSONObject jasonObject = (JSONObject) inputFileJsonList;
        return jasonObject;
    }

    /**
     * Initialise the company List from the extracted Json
     * @param companyJson
     * @return the initialized company list
     */
    public List<Company> initializeCompany(JSONArray companyJson) {
        List<Company> listCompanyToBuild = new ArrayList<>();
        companyJson.forEach(company -> {
            Company companyToBuild = new Company(Integer.valueOf(((JSONObject) company).get(Constant.FIELD_ID).toString()),
                    Double.valueOf(((JSONObject) company).get(Constant.FIELD_BALANCE).toString()),
                    ((JSONObject) company).get(Constant.FIELD_NAME).toString());
            listCompanyToBuild.add(companyToBuild);
        });
        return listCompanyToBuild;
    }

    /**
     * Initialise the user List from the extracted Json
     * @param userJson
     * @return the initialized user list
     */
    public List<User> initializeUser(JSONArray userJson, JSONArray walletJson) {
        List<User> listUserToBuild = new ArrayList<>();
        userJson.forEach(user -> {
            JSONArray balanceJson = (JSONArray) ((JSONObject) user).get(Constant.FIELD_BALANCE);
            List<Wallet> userWallet = initialiseUserWallet(balanceJson, walletJson);
            User userToBuild = new User(Integer.valueOf(((JSONObject) user).get(Constant.FIELD_ID).toString()), new ArrayList<>(), userWallet);
            listUserToBuild.add(userToBuild);
        });
        return listUserToBuild;
    }

    /**
     * Initialise the user wallet List from the extracted Json
     * @param balanceJson
     * @param walletJson
     * @return the initialized user wallet list
     */
    public List<Wallet> initialiseUserWallet(JSONArray balanceJson, JSONArray walletJson) {
        List<Wallet> listWalletToBuild = new ArrayList<>();
        walletJson.forEach(wallet -> {
            Wallet walletToBuild = new Wallet(Integer.valueOf(((JSONObject) wallet).get(Constant.FIELD_ID).toString()),
                    0);
            balanceJson.forEach(userBalanceWallet -> {
                if (StringUtils.equalsIgnoreCase(((JSONObject) wallet).get(Constant.FIELD_ID).toString(),
                        ((JSONObject) userBalanceWallet).get(Constant.FIELD_WALLET_ID).toString())) {
                    walletToBuild.setAmount(Double.valueOf(((JSONObject) userBalanceWallet).get(Constant.FIELD_AMOUNT).toString()));
                }
            });
            listWalletToBuild.add(walletToBuild);
        });

        return listWalletToBuild;
    }

    /**
     * Method which write an output.json from the companies and users
     * @param listCompany
     * @param listUser
     * @throws IOException
     */
    public void writeInputJsonFile(List<Company> listCompany, List<User> listUser) throws IOException {
        JSONArray listCompanyJson = generateCompanyJson(listCompany);
        JSONArray listUserJson = generateUserJson(listUser);
        JSONArray listDistributionJson = generateDistributionJson(listUser);

        JSONObject jsonToWrite = new JSONObject();
        jsonToWrite.put(Constant.COMPANIES, listCompanyJson);
        jsonToWrite.put(Constant.USERS, listUserJson);
        jsonToWrite.put(Constant.DISTRIBUTIONS, listDistributionJson);
        FileWriter file = new FileWriter("output.json");
        file.write(jsonToWrite.toJSONString());
        file.flush();
    }

    /**
     * Generate the company Json to write
     * @param listCompany
     * @return the converted company Json to write
     */
    private JSONArray generateCompanyJson(List<Company> listCompany) {
        JSONArray listCompanyJson = new JSONArray();
        listCompany.stream().forEach(company -> {
            JSONObject companyJson = new JSONObject();
            companyJson.put(Constant.FIELD_ID, company.getId());
            companyJson.put(Constant.FIELD_NAME, company.getName());
            companyJson.put(Constant.FIELD_BALANCE, company.getBalance());
            listCompanyJson.add(companyJson);
        });
        return listCompanyJson;
    }

    /**
     * Generate the user Json to write
     * @param listUser
     * @return the converted user Json to write
     */
    private JSONArray generateUserJson(List<User> listUser) {
        JSONArray listUserJson = new JSONArray();
        listUser.stream().forEach(user -> {
            JSONObject userJson = new JSONObject();
            userJson.put(Constant.FIELD_ID, user.getId());
            if (user.getListWallet() == null || user.getListWallet().isEmpty()) {
                userJson.put(Constant.FIELD_BALANCE, new JSONArray());
            } else {
                JSONArray listWalletJson = new JSONArray();
                user.getListWallet().stream().forEach(wallet -> {
                    if(user.getUserBalanceByType(wallet.getId()) < 1) {
                        return;
                    }
                    JSONObject walletJson = new JSONObject();
                    walletJson.put(Constant.FIELD_WALLET_ID, wallet.getId());
                    walletJson.put(Constant.FIELD_AMOUNT, user.getUserBalanceByType(wallet.getId()));
                    listWalletJson.add(walletJson);
                });
                userJson.put(Constant.FIELD_BALANCE, listWalletJson);
            }
            listUserJson.add(userJson);
        });

        return listUserJson;
    }

    /**
     * Generate the distribution Json to write
     * @param listUser
     * @return the converted distribution Json to write
     */
    private JSONArray generateDistributionJson(List<User> listUser) {
        JSONArray listDistributionJson = new JSONArray();
        List<Distribution> listDistributionOfAllUser = new ArrayList<>();
        listUser.stream().forEach(user -> {
            if(user.getListDistribution() == null || user.getListDistribution().isEmpty()) {
                return;
            }
            listDistributionOfAllUser.addAll(user.getListDistribution());
        });

        listDistributionOfAllUser.stream().sorted(Comparator.comparingInt(Distribution::getId)).forEach(distribution -> {
            JSONObject distributionJson = new JSONObject();
            distributionJson.put(Constant.FIELD_ID, distribution.getId());
            distributionJson.put(Constant.FIELD_WALLET_ID, distribution.getIdWallet());
            distributionJson.put(Constant.FIELD_AMOUNT, distribution.getAmount());
            LocalDate startLocalDate = LocalDate.from(distribution.getStartDate().toInstant().atZone(ZoneOffset.UTC));
            String startDateString = DateTimeFormatter.ISO_DATE.format(startLocalDate);
            distributionJson.put(Constant.FIELD_START_DATE, startDateString);
            LocalDate endLocalDate = LocalDate.from(distribution.getEndDate().toInstant().atZone(ZoneOffset.UTC));
            String endDateString = DateTimeFormatter.ISO_DATE.format(endLocalDate);
            distributionJson.put(Constant.FIELD_END_DATE, endDateString);
            distributionJson.put(Constant.FIELD_COMPANY_ID, distribution.getIdCompany());
            distributionJson.put(Constant.FIELD_USER_ID, distribution.getIdUser());
            listDistributionJson.add(distributionJson);
        });
        return listDistributionJson;
    }
}
