package wedoogift.challenge.level2.solution.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wedoogift.challenge.level2.solution.model.Company;
import wedoogift.challenge.level2.solution.model.Distribution;
import wedoogift.challenge.level2.solution.model.User;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Yang SHI the 08/02/2021
 * This class represents the service for balance transaction
 */

@Service
public class BalanceTransactionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceTransactionService.class);

    @Autowired
    CompanyService companyService;

    @Autowired
    DistributionService distributionService;

    /**
     * This method allows a company to give a distribution to a user
     *
     *
     * @param company which gives the distribution
     * @param user who get the distribution
     * @param amount of money the user get from the distribution
     * @param idDistribution technical id of the distribution
     * @param startDate the distribution start date
     * @param type the type of distribution, could be a Gift or a Meal
     */
    public void giveGiftCard(Company company, User user, double amount, int idDistribution, Date startDate, int type) {
        if(company == null || user == null) {
            LOGGER.info("Company or user is null. Exiting the transaction");
            return;
        }
        double oldCompanyBalance = company.getBalance();
        company = companyService.changeCompanyBalance(company, -amount);
        if(company.getBalance() == oldCompanyBalance) {
            LOGGER.info("No change for the company#{} balance", company.getName());
            return;
        } else {
            Distribution distribution = distributionService.buildNewDistribution(idDistribution, company, user, amount, startDate, type);
            if(user.getListDistribution() == null) {
                user.setListDistribution(new ArrayList<>());
            }
            user.getListDistribution().add(distribution);
        }
    }
}
