package wedoogift.challenge.level2.solution.utils;


public class Constant {
    public final static double MINIMUM_BALANCE_VALUE = 0;
    public final static long END_DATE_DAY_NUMBER = 366;
    public final static int GIFT_TYPE_DISTRIBUTION_ID = 1;
    public final static int MEAL_TYPE_DISTRIBUTION_ID = 2;
    public final static int END_DATE_FOR_MEAL_TYPE_DISTRIBUTION_MONTH = 2;
    public static String FIELD_ID = "id";
    public static String FIELD_NAME = "name";
    public static String FIELD_BALANCE = "balance";
    public static String FIELD_WALLET_ID = "wallet_id";
    public static String FIELD_AMOUNT = "amount";
    public static String FIELD_START_DATE = "start_date";
    public static String FIELD_END_DATE = "end_date";
    public static String FIELD_COMPANY_ID = "company_id";
    public static String FIELD_USER_ID = "user_id";
    public static String COMPANIES = "companies";
    public static String USERS = "users";
    public static String DISTRIBUTIONS = "distributions";
}
