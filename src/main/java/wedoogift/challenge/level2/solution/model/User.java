package wedoogift.challenge.level2.solution.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Created by Yang SHI the 07/02/2021
 * This class represents the model of user
 */

@Data
@AllArgsConstructor
public class User {
    private int id;
    private List<Distribution> listDistribution;
    private List<Wallet> listWallet;

    /**
     * Get the user wallet balance depending of the type of wallet.
     * Only count the amount of distribution which have a valid date
     *
     * @param type wallet type
     * @return the amount of balance of the wallet type
     */
    public double getUserBalanceByType(int type) {
        Date currentDate = new Date();
        double walletBalance = (listWallet == null || listWallet.isEmpty()) ? 0 : listWallet.stream().filter(wallet -> wallet.id == type).mapToDouble(Wallet::getAmount).sum();
        return walletBalance + listDistribution.stream()
                .filter(distribution -> distribution.getIdWallet() == type
                        && ((currentDate.after(distribution.getStartDate()) && currentDate.before(distribution.getEndDate()))
                        || currentDate.equals(distribution.getStartDate()) || currentDate.equals(distribution.getEndDate())))
                .mapToDouble(Distribution::getAmount).sum();
    }
    
}
