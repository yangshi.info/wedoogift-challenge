package wedoogift.challenge.level2.solution.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Yang SHI the 07/02/2021
 * This class represents the model of company
 */


@Data
@AllArgsConstructor
public class Company {
    private int id;
    private double balance;
    private String name;
}
