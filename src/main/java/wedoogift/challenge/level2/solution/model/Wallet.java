package wedoogift.challenge.level2.solution.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Yang SHI the 09/02/2021
 * This class represents the model of wallet
 */

@Data
@AllArgsConstructor
public class Wallet {
    int id;
    double amount;
}
