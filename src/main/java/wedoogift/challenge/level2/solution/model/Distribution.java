package wedoogift.challenge.level2.solution.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * Created by Yang SHI the 07/02/2021
 * This class represents the model of distribution
 */

@Data
@AllArgsConstructor
public class Distribution {
    private int id;
    private double amount;
    private int idWallet;
    private Date startDate;
    private Date endDate;
    private int idCompany;
    private int idUser;
}
