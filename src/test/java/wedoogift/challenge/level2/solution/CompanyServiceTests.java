package wedoogift.challenge.level2.solution;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import wedoogift.challenge.level2.solution.model.Company;
import wedoogift.challenge.level2.solution.service.CompanyService;

import static org.junit.Assert.assertTrue;

@SpringBootTest
class CompanyServiceTests {

	@Autowired
	CompanyService companyService;

	@Test
	void testChangeCompanyBalance() {
		Company testCompany = new Company(1, 200, "Company to test");
		Company companyTest1 = new Company(1, 400, "Company to test");
		Company companyTest2 = new Company(1, 300, "Company to test");
		testCompany = companyService.changeCompanyBalance(testCompany, 200);
		assertTrue(testCompany.equals(companyTest1));
		testCompany = companyService.changeCompanyBalance(testCompany, -100);
		assertTrue(testCompany.equals(companyTest2));
		testCompany = companyService.changeCompanyBalance(testCompany, -400);
		assertTrue(testCompany.equals(companyTest2));
	}

}
