package wedoogift.challenge.level2.solution;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import wedoogift.challenge.level2.solution.model.Company;
import wedoogift.challenge.level2.solution.model.Distribution;
import wedoogift.challenge.level2.solution.model.User;
import wedoogift.challenge.level2.solution.service.DistributionService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import static org.junit.Assert.assertTrue;

@SpringBootTest
class DistributionServiceTests {

	@Autowired
	DistributionService distributionService;

	@Test
	void testBuildNewDistribution() throws ParseException {
		Company testCompany = new Company(1, 200, "Company to test");
		User testUser = new User(2, new ArrayList<>(), new ArrayList<>());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.FRENCH);
		Distribution distributionToCompare1 = new Distribution(0, 200, 1, formatter.parse("2020-09-16"), formatter.parse("2021-09-17"), 1, 2);
		Distribution distributionToCompare2 = new Distribution(0, 300, 2, formatter.parse("2020-09-16"), formatter.parse("2021-02-28"), 1, 2);
		Distribution distributionToTest1 = distributionService.buildNewDistribution(0, testCompany, testUser, 200, formatter.parse("2020-09-16"), 1);
		assertTrue(distributionToTest1.equals(distributionToCompare1));
		Distribution distributionToTest2 = distributionService.buildNewDistribution(0, testCompany, testUser, 300, formatter.parse("2020-09-16"), 2);
		assertTrue(distributionToTest2.equals(distributionToCompare2));
	}

}
